<?php


/*
[SECTION] Repetition Control Structures/Loops

	- are used to execute code multiple times


	While Loop:

		A While loop takes a single condition. For as long as the condition is true, the code inside the block will run.

*/


	function whileLoop(){

		$count = 5;

		while($count !==0){

			echo $count . '</br>';
			$count--;
		}
	}


/*
	Do-While Loop:

		- A do-while loop works a lot like a while loop, except a do-whilte loops will always execute the givewn code block once before the loop begins. (and conditions are checked)


*/


	function doWhileLoop(){

		$count = 20;


		do{

			echo $count . '</br>';
			$count--;

		}while ($code > 20);
	}



/*
	For Loop
	
		- A for loop is more flexible than a while or do-while loop, because it is self-contained (the counter inside of the parenthesis only exists for that specific loop)

*/


	function forLoop(){

		for($count = 0; $count <= 20; $count++){

			echo $count . '</br>';

		}
	}


/*
	The "continue" and "break" keywords can also be used in PHP loops the same way they are used in JS


	Continue: Allows code to go to the next loop/skip the current iteration withouty finishing the current code block

	Break: End the execution of the loop



[SECTION] Array Manipulation

	- An array is a data structure that can hold multiple related values.
	- Arrays are declared using square brackets [] or array() function

*/

	$studentNumbers = array('1923', '1924', '1925', '1926');
	$studentNumbers = ['1923', '1924', '1925', '1926'];



	// Simple Arrays

	$grades = [98.5, 94.3, 89.2, 90.1];



	// Associatuve Arrays

		// An array that allows each element to have a unique string key

	
	$gradePeriods = ['firstGrading' => 98.5, 'secondGrading' => 94.3, 'thirdGrading' => 89.2, 'fourthGrading' => 90.1];



	// Multi-Dimentional Arrays

		// array within an array

	$heroes = [
		['Ironman', 'Thor', 'Hulk'],
		['Wolverine', 'Cyclops', 'Jean Grey'],
		['Batman', 'Superman', 'Wonder Woman']
	];



	// Multi-Diementional Associative Array

	$powers = [

		'regular' => ['Repulsor Blast', 'Rocket Puch'],
		'signature' => ['Unibeam']

	];



	$computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];
















